#!/usr/bin/perl

if($ARGV[0] eq "" || $ARGV[1] eq "")
{
        printf "SYNTAX: transcr2phones_text.pl fin_letters2phones  transcript \n";
        exit(1);
}

#$list_of_files = $ARGV[0];
#$mlf = $ARGV[1];
$transcript = $ARGV[0];
$fin_let_ph = $ARGV[1];

=for comment
#load file names from {train,cv,test}.scp
open LIST, "$list_of_files" or die "ERROR: Can not open the list of files: $list_of_files.";
$i = 0;
while($l = <LIST>)
{
        chomp $l;
        if($l =~ /\/([^\/]+$)/)          #extract path
        {
                $fname = $1;
        }
        else
        {
                $fname = $l;
        }
        if($fname =~ /^(.+)\.[^\.]+$/)   #extract extension
        {
                $fname = $1;
        }
        $files_table{$i} = $fname;
#        printf "$fname \n";
        $i++;
}
close LIST;
=cut

# load and parse MLF
open TXT, "$transcript" or die "ERROR: Can not open the transcript file: $transcript.";

while($file = <TXT>)
{
        chomp $file;
#        $file = $1;

=for comment        
	$file =~ /\"(.+)\"/;
        $file = $1;
        if($file =~ /\/([^\/].+)$/)          #extract path
        {
                $file = $1;
        }
        if($file =~ /^(.+)\.[^\.]+$/)        #extract extension
        {
                $file = $1;
        }
=cut
        @uttid_array = ();
        @text_array = ();
        if($file =~ /:$/)
        {
          push @uttid_array, $1;
        }

        do
        {
                $l = <TXT>;
                chomp $l;                
                push @text_array, $l;


#                ($start, $stop, $phn, $oth) = split /\s+/, $l, 4;                

        }
        while($l =~ /:$/);

        printf "@text_array\n\n";

#	printf "$file \n"; # print ids of files form mlf
#        printf "\n\n---";
#        printf "@phonemes_array\n\n";
# store
#        $starts{$file} = "@starts_array";
#        $stops{$file} = "@stops_array";
#        $phonemes{$file} = "@phonemes_array";
}
close TXT;

=for comment
# go file by file (sentence by sentence) and create labels
for($i = 0; $i < (keys %files_table) + 0; $i++)
{
        $file = $files_table{$i}.".story-bt";
        die "ERROR: There is less file names in the file list than the number of sections in the pfile." if(not defined($files_table{$i}));
#        $nframes = $frames_table{$i};

        printf "$files_table{$i} ";

        die "ERROR: There are not labels in the MLF for the file: $file." if(not defined($phonemes{$file}));
        @starts_array = split / /, $starts{$file};
        @stops_array = split / /, $stops{$file};
        @phonemes_array = split / /, $phonemes{$file};

	printf "@phonemes_array\n";
}
=cut
