#!/bin/bash

mkdir -p data/local
local=`pwd`/local
scripts=`pwd`/scripts

export PATH=$PATH:`pwd`/../../../tools/irstlm/bin

echo "Preparing data for aligning"
test_base_name=test_ali
waves_dir=$1

cd data/local

# read just names with ext (utt.wav)
#ls -1 ../../$waves_dir | sort -u > waves.list
ls -1 ../../$waves_dir > waves.list

# just name and path (e.g.: 1_0_0_0_0_0_0_0 waves_yesno/1_0_0_0_0_0_0_0.wav)
../../local/create_yesno_wav_scp.pl ${waves_dir} waves.list > ${test_base_name}_wav.scp

# TODO

# arpa format language model
#cp ../../input/task.arpabo lm_tg.arpa

cd ../..

if [ ! -f data/local/test_ali.txt ]; then
  echo "ERROR please put you your wav phone transcription in data/local/test_ali.txt";  
fi

# This stage was copied from WSJ example
for x in test_ali; do 
  mkdir -p data/$x
  cp data/local/${x}_wav.scp data/$x/wav.scp
  cp data/local/$x.txt data/$x/text
  cat data/$x/wav.scp | awk '{printf("%s global\n", $1);}' > data/$x/utt2spk
  utils/utt2spk_to_spk2utt.pl <data/$x/utt2spk >data/$x/spk2utt
done
echo "Prepared Successfuly"
