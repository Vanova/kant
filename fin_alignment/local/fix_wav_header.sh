#!/bin/bash

# loop all bad wav files and fix these headers
for x in forAlignTest/bad_wav/*.wav; do
  outf=$(basename $x)  
  python forAlignTest/fix_header.py $x forAlignTest/$outf
done
