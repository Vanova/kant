#!/bin/bash

mkdir -p data/local/dict

#cp input/lexicon_nosil.txt data/local/dict/lexicon_words.txt

#cp input/lexicon.txt data/local/dict/lexicon.txt

cat input/phones_eng.txt | grep -v sil | sort > data/local/dict/nonsilence_phones.txt

echo "sil" > data/local/dict/silence_phones.txt

echo "sil" > data/local/dict/optional_silence.txt

echo "Dictionary preparation succeeded"
