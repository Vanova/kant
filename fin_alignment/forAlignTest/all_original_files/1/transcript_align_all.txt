Transcript for speech files.

TS1_1.wav, ImpB_imiTS1_1.wav, ImpB_imiTS1_6.wav:
Kyllä mä nuukuhdin pääsin siellä nukkumaan  puoli kakstoista ja ja tuota kyllä mä nukkuin ihan sanollisesti herääsoi pari kerta  mutta kansi kylke ja... mullä on ehka ekha semonen armon lahja että mä pyystyn päivälläkin jos jos jos tuntu että väsyttä  minä voin käy...

TS1_3.wav ImpB_imiTS1_3.wav ImpB_imiTS1_8.wav:
käydä pitkalle ja siten päätan etta minä nukku ja minä nukkun  vistoista minutin tai puolituntia ja sitten virkena nousen ylös muutten mä usko että en olisi tällä teidän vastapaata nyt keskustelemassa jos ei tata tata kyky ole.  Konessa  lentokonessa yölenno ei nukku erityisesti hyvin mutta siten varan seurevana päivänä

TS1_5.wav ImpB_imiTS1_5.wav ImpB_imiTS1_10.wav:
aika etta painun  nukumaan heti kun aamulla tulan perille esimerkiksi kun lensi Singapore joku aika siten.  Tä tapa milla minä joudun elämän niin ja liikuman paljon ni se edelluta erinomaisen kurinalaista elävä ei mitä ila ilan istujäisiä ja..



TS6_1.wav ImpB_imiTS6_1.wav ImpB_imiTS6_7.wav:
Tekemättäko..jäivat kaikki lähti lomalle ja mä aattelin ja mä olin just löytänyt kaikki tyypit Hanoihin niin..se jäi vähän tekemättä ja nyt me tehdään se. Oli fantastista kun me skulattiin nyt tossa ekaa kertaa kimpas kolmeenkyt plus vuoteen...

TS6_3.wav ImpB_imiTS6_3.wav ImpB_imiTS6_9.wav:
että huones oli selanen  tiedät sä magiikka se on kun kaikkien henkilökohtaiset niin kun personalities sopii yhteen siten tuli oikein balanssi joka on harvinaista bändeissä ja mä

TS6_5.wav ImpB_imiTS6_5.wav ImpB_imiTS6_11.wav:
hifassi heti miks siitä tuli niin iso kun sita tuli mitä mä silloin tajunnu. Sillä mottoritie on kuuma siitä hän on tullut jotain joka on melkein kansanlaulu sä voit menä karaoke mestaa…



TS7_1.wav ImpB_imiTS7_1.wav ImpB_imiTS7_8.wav:
Niin Melardinhan on on selanen tässä tapauksessa niin kun ehkä Savosta  muuttunut muuttanut Helsinkiin tämmönen Stadin stadin kundi joka tota on tottunut olemaan putkassa päinvastoin kun Nikander joka joutui tämmöisen pikku väkivaltapuuskan saatu ensimäistä kertaa putkaa ja...

TS7_3.wav ImpB_imiTS7_3.wav ImpB_imiTS7_10.wav:
kyllähän Melardi on sellanen niin ku hyvä sydäminen äijä jolla on tota vaimo ja lapsia se on työtön varmaankin osittain omastakin syystä että on viinaan menevä ja

TS7_5.wav ImpB_imiTS7_5.wav ImpB_imiTS7_12.wav:
vieläkin kuitenkin nuori nuori voimakas äijä joka kuitenkin rakastaa perhettään ja pitää huolta perhestään asuu omakotitalossa mutta työttömänä tietenki on vähän sellanen vailla vailla niin ku juuria yhteiskuntaan ja

TS7_7.wav ImpB_imiTS7_7.wav ImpB_imiTS7_14.wav:
ja mutta kuitenkin se se mitä se on myös se on se on kilttiä, mukava ja hyvin tällainen ajatelevainen huolehtivainen mutta elämän tilanne sen vuoksi ajanut vähän tällaiseen sieltä tavalla veitsen terälle että  löytyy aika usein putkassa ilmeisesti..



TS8_1.wav ImpB_imiTS8_1.wav ImpB_imiTS8_7.wav:
No tästä asiasta on kuulemma ollu kahta mieltä että minä kuulemma vaan niin kun lähinnä peloitelen mutta mutta  asia on niin että rovio sytyy sen takia että lopultakin jotakin pientä tässä elokuvamaailmassa rupee tapahtumaan ja toistaseksi siitä  syystä että  aina kun olen elokuvan tehnyt niin lehdistö on sanottu etta...

TS8_3.wav ImpB_imiTS8_3.wav ImpB_imiTS8_9.wav:
se elokuva on huono no eihä mitään huonoa kannata säilyttää ja toisekseen mulla on niin paljo jo varastonti vaikkeuksiakin että ruvetaan vaan sieltä takapäästä  poltamaan pois ettei tule turhaa tungosta... Kolme ensimmäistä seuraavan kerran lähtee sitten loput  nimitäin sit taas ne vuokrat ovat aika suuret näissa varasto  varasto tiloissa nykyisin että  jokun ne huonoo ovat niin mitä niitä sailyttää eiks nii.

TS8_5.wav ImpB_imiTS8_5.wav ImpB_imiTS8_11.wav:
Mulle riittää täysin se  että elokuvan katsojat niitä arvostaa jos lahtee sille linjalle että vaan niitä pelkästään arvostaa säätiö ...niin ja rakentaa koko homman sille...jos kerran käy niin ettei säätiöltä saakkaan niin ei yleisö ei niitä myöskään katso.  Mä lähden liikeelle siltä pohjalta vaan että 


