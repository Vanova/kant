#!/usr/bin/python
# python <in> <out>
# NB: usage see local/fix_wav_header.sh

import wave, sys

inName = sys.argv[1]
outName = sys.argv[2]

print '[info] Input file: ', inName,', Out file: ', outName

fin = wave.open(inName, 'rb')
framerate = fin.getframerate()
frames = fin.getnframes()
channels = fin.getnchannels()
width = fin.getsampwidth()
ps = fin.getparams()
data = fin.readframes(frames)

print("sampling rate = {framerate} Hz, length = {frames} samples, channels = {channels}, sample width = {width} bytes".format(**locals()))

fout = wave.open(outName,'wb')
fout.setparams(ps)
fout.writeframes(data)

fout.close()
fin.close()
