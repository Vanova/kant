#!/bin/bash

. ./cmd.sh 
[ -f path.sh ] && . ./path.sh
set -e

# check folders exist
if [ ! -d forAlignTest/wav ]; then
  echo "ERROR please put your wav in forAlignTest/wav";
fi

if [ ! -d data/train_timit ]; then
  echo "ERROR please copy data from (trained model) TIMIT data/train to data/train_timit";
fi

echo ============================================================================
echo "                Data & Lexicon & Language Preparation                     "
echo ============================================================================
# remove previous results
# rm -rvf data/test_ali mfcc data-fmllr-tri3 exp/dnn4_pretrain-dbn_dnn_ali

test_ali_dir=data/test_ali
ali_base=forAlignTest
ali_wav=${ali_base}/wav
train_data=data/train_timit

# NOTE: you need to prepare and put transcriptions
# (from Finnish words to phonemes) to data/local/test_ali.txt
# in order to align on phonemes level
local/prepare_data.sh $ali_wav 
local/prepare_dict.sh

# For monophone alignment
# Copy model files needed for alignment:
# exp/mono/tree exp/mono/final.mdl exp/final.occs

# NOTE: the next folders were copied from the trained model on TIMIT.
# If these exists then no worries.
#model=mono
#mkdir exp/$model
#cp ../timit/s5/exp/$model/tree ./exp/$model/
#cp ../timit/s5/exp/$model/final.mdl ./exp/$model/
#cp ../timit/s5/exp/$model/final.occs ./exp/$model/

##############################################
utils/validate_data_dir.sh --no-feats data/test_ali
##############################################

echo =====================================================================
echo "               MFCC feature extraction for monophone model         "
echo =====================================================================
# MFCC feature extraction for monophones
x=test_ali
steps/make_mfcc.sh --nj 1 data/$x exp/make_mfcc/$x mfcc 
steps/compute_cmvn_stats.sh data/$x exp/make_mfcc/$x mfcc 

# NOTICE: if there are problems with wav files headers
# local/fix_wav_header.sh

# Check folder with features
utils/validate_data_dir.sh data/test_ali
#############################################

echo =====================================================================
echo "               Preprocessing for DNN model                         "
echo =====================================================================
# MFCC features for DNN model
data_fmllr=data-fmllr-tri3
dir=$data_fmllr/test_ali
srcdir=data/test_ali
gmmdir=exp/tri3
logdir=$dir/log
feadir=$dir/data

# NOTE: the next folders were copied from DNN model trained on TIMIT.
# If these exists then no worries.
# mkdir -p $dir $logdir $feadir $gmmdir
# copy needed files from trained TIMIT model
# cp ../timit/s5/$gmmdir/cmvn_opts $gmmdir/
# cp ../timit/s5/$gmmdir/splice_opts $gmmdir/
# cp ../timit/s5/$gmmdir/final.mat $gmmdir/ # for LDA features

# MFCC feature extraction for triphone DNN model (trained on TIMIT dataset)
steps/nnet/make_fmllr_feats_attrib.sh --nj 1 --cmd "$train_cmd" $dir $srcdir $gmmdir $logdir $feadir || exit 1

echo =====================================================================
echo "               Alignment on DNN model                              "
echo =====================================================================
data_fmllr=data-fmllr-tri3
dir=exp/dnn4_pretrain-dbn_dnn_smbr
srcdir=exp/dnn4_pretrain-dbn_dnn
acwt=0.2
# delete previous experiments
#rm -rf ${srcdir}_ali

# NOTE: you need to prepare and put transcriptions
# (from Finnish words to phonemes) to data/local/test_ali.txt
# in order to align on phonemes level

# First we generate lattices and per-frame alignments:
# NOTE: if has GPU, use --use-gpu yes
steps/nnet/align.sh --use-gpu no --align-to-lats true --nj 1 --cmd "$train_cmd" $data_fmllr/test_ali data/lang_timit $srcdir ${srcdir}_ali || exit 1;

# transform lattice to phonemes time alignment ark:gunzip -c $dir/lat.JOB.gz|
lattice-1best --acoustic-scale=0.1 ark:exp/dnn4_pretrain-dbn_dnn_ali/lat.1 ark:- |\
  lattice-align-phones --replace-output-symbols=true exp/dnn4_pretrain-dbn_dnn_ali/final.mdl ark:- ark:- |\
  nbest-to-ctm ark:- ali_result/1.ctm

# ctm int labels to symbol phones
symtab=data/lang_timit/phones.txt
phonemap=conf/phones.60-48-39.map

utils/int2sym.pl -f 5- $symtab ali_result/1.ctm > ali_result/1_sym.ctm
utils/int2sym.pl -f 5- $symtab ali_result/1.ctm | local/timit_norm_trans.pl -i - -m $phonemap -from 48 -to 39 > ali_result/1_sym_norm39.ctm

# alignment to time stamps
#ali-to-phones exp/dnn4_pretrain-dbn_dnn_ali/final.mdl ark:exp/dnn4_pretrain-dbn_dnn_ali/ali.1 ark,t:ali_result/ali_per_frame.tra
#utils/int2sym.pl -f 2-  $symtab ali_result/ali_per_frame.tra | local/timit_norm_trans.pl -i - -m $phonemap -from 48 -to 39 > ali_result/ali_per_frame_norm39.ctm
echo " ======== [DONE] ========"

