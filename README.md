# KANT: Kaldi speech AlignmeNT example
This repository contains example for aligning Finnish phoneme transcriptions to 
speech signals using DNN trained on TIMIT dataset (English dataset) on phone level. 
...

# How to run?

1. fix path to your installed Kaldi toolkit in `path.sh`
2. be sure that all your bash files are runnable, fixing: run from the project folder `chmod -R +x ./`
3. run commands line by line from `run_system.sh` and check that you get expected result.

# Scripts description
TO BE CONTINUED...